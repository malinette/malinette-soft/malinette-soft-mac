#!/bin/bash
# Launch script (Mac OS X)

# Prevent Resume (from egregore project)
rm -Rf ~/Library/Saved\ Application\ State/org.puredata*
defaults write org.puredata NSQuitAlwaysKeepsWindows -int 0
defaults write org.puredata NSQuitAlwaysKeepsWindows -bool false
defaults write org.puredata.pd.wish NSQuitAlwaysKeepsWindows -int 0
defaults write org.puredata.pd.wish NSQuitAlwaysKeepsWindows -bool false

# Get absolute path
#DIR="$(cd "$(dirname "$0")" && pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pure Data binary
PD="Pd.app/Contents/Resources"

# Launch Pd and settings
"$DIR"/"$PD"/bin/pd -helppath "$DIR"/"$PD"/doc/5.reference -font-weight normal -open "$DIR"/malinette-ide/MALINETTE.pd -path "$DIR"/"$PD"/extra -path "$DIR"/externals -path "$DIR"/malinette-ide/abs/brutbox -path "$DIR"/malinette-ide/abs/malinette-abs -path "$DIR"/malinette-ide/tools/tclplugins/malinette-menu -path "$DIR"/externals/bassemu~ -path "$DIR"/externals/comport -path "$DIR"/externals/creb -path "$DIR"/externals/cyclone -path "$DIR"/externals/ext13 -path "$DIR"/externals/Gem -path "$DIR"/externals/ggee -path "$DIR"/externals/hcs -path "$DIR"/externals/iemguts -path "$DIR"/externals/iemlib -path "$DIR"/externals/list-abs -path "$DIR"/externals/mapping -path "$DIR"/externals/markex -path "$DIR"/externals/maxlib -path "$DIR"/externals/moocow -path "$DIR"/externals/moonlib -path "$DIR"/externals/motex -path "$DIR"/externals/pduino -path "$DIR"/externals/pmpd -path "$DIR"/externals/puremapping -path "$DIR"/externals/purepd -path "$DIR"/externals/sigpack -path "$DIR"/externals/tof -path "$DIR"/externals/zexy -path "$DIR"/externals/hid -path "$DIR"/externals/completion-plugin -lib Gem -lib zexy -lib iemlib -lib iem_t3_lib
