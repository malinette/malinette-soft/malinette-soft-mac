# malinette-soft
Open Source Kit For Programming Interactivity. 

## Description
Standalone version of the [malinette](http://malinette.info) project. It contains Pure Data, 21 libraries, malinette-ide and a startup script. To stay up-to-date, you can replace the [malinette-ide](https://framagit.org/malinette/malinette-ide) folder.

## Installation
- Download
- Extract
- Launch "start-malinette.sh" script

On Mac OS, you should pass through the "Impossible to open Pd.app" window. The common way is to right click on "Pd.app" and open it. Or you could also go to "System Preferences > Security and confidentiality" and allow "Pd.app".

## Launcher
You can add an alias or a launcher to make easiest the way to launch La Malinette. On Mac OS : right click on desktop and create an alias. You can drag it into the "Documents" part of the Dock.
